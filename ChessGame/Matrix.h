#pragma once
#include <vector>
using std::vector;
using std::pair;
using std::make_pair;
template <class T>
class Matrix
{
	// 2D vector stored in a 1D vector, stored left to right then top to bottom
	// therefore, access to pos (20,10) in a (50,50) Matrix is translated to -> 20 + 10 * 50
protected:
	vector<T> v;
	unsigned hSize;
public:
	Matrix();
	Matrix(unsigned xSize, unsigned ySize, T defaultValue);

	unsigned xSize()const;
	unsigned ySize()const;
	unsigned size()const;


	const T& get(unsigned x, unsigned y)const;
	void set(unsigned x, unsigned y, T value);

	// unprotected methods
	T& GET(unsigned x, unsigned y);
	vector<T>& GET();
	void setHSize(unsigned hSize);

	void resize(unsigned x, unsigned y, T defaultValue);

	pair<unsigned, unsigned> pos(unsigned)const;
	unsigned pos(unsigned x, unsigned y)const;
};

template<class T>
Matrix<T>::Matrix()
{
}

template<class T>
Matrix<T>::Matrix(unsigned xSize, unsigned ySize, T defaultValue)
{
	resize(xSize, ySize, defaultValue);
}

template<class T>
unsigned Matrix<T>::xSize() const
{
	return hSize;
}

template<class T>
unsigned Matrix<T>::ySize() const
{
	return v.size() / hSize;
}

template<class T>
unsigned Matrix<T>::size() const
{
	return v.size();
}

template<class T>
const T& Matrix<T>::get(unsigned x, unsigned y) const
{
	return v[pos(x, y)];
}

template<class T>
void Matrix<T>::set(unsigned x, unsigned y, T value)
{
	v[pos(x, y)] = value;
}

template<class T>
T& Matrix<T>::GET(unsigned x, unsigned y)
{
	return v[pos(x, y)];
}

template<class T>
vector<T>& Matrix<T>::GET()
{
	return v;
}

template<class T>
void Matrix<T>::setHSize(unsigned hSize)
{
	this->hSize = hSize;
}

template<class T>
void Matrix<T>::resize(unsigned x, unsigned y, T defaultValue)
{
	v.resize(x*y, defaultValue);
	hSize = x;
}

template<class T>
pair<unsigned, unsigned> Matrix<T>::pos(unsigned pos) const
{
	return std::make_pair(pos%xSize, pos / xSize);
}

template<class T>
unsigned Matrix<T>::pos(unsigned x, unsigned y) const
{
	return x + y * hSize;
}

