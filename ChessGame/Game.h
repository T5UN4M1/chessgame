#pragma once
#include "Matrix.h"
#include "Piece.h"
#include "Move.h"
#include <memory>
#include <vector>
#include <SFML/Graphics.hpp>

using std::unique_ptr;
using std::vector;
class Game
{
public:
	Game();

	Matrix<unique_ptr<Piece>> matrix;

	Color turn;
	vector<Move> moves;



	bool isInside(sf::Vector2u pos)const;
	bool isInside(unsigned x, unsigned y)const;
	bool hasPiece(sf::Vector2u pos)const;
	bool hasEnemyPiece(sf::Vector2u& pos,Piece& piece)const;
	Piece& getPiece(sf::Vector2u pos)const;

	Moves getMoves(const sf::Vector2u& pos)const;
	bool isLegal(const Move& move)const;
	bool move(const Move& move);



	void setupGame();
};

