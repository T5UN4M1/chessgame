#include "BoardView.h"



BoardView::BoardView(Game& game):game(game)
{
	w = std::make_unique<sf::RenderWindow>(sf::VideoMode(800,800), "Chess", sf::Style::Default);
}

void BoardView::init()
{
	tPieces = sf::Texture();
	tPieces.loadFromFile("./res/pieces.png");
}

void BoardView::resetDisplay()
{
	sf::Vector2f cs = sf::Vector2f(100, 100);

	board = sf::VertexArray(sf::Quads,8*8 * 4);
	caseEffect = sf::VertexArray(sf::Quads, 0 * 0 * 4);
	pieces = sf::VertexArray(sf::Quads,8*8 *  4);
	// design board and pieces
	for (unsigned y = 0; y < 8; ++y) {
		for (unsigned x = 0; x < 8; ++x) {
			unsigned cell = (y * 8 + x) *4;

			board[cell    ].position = sf::Vector2f(cs.x * x, cs.y * y);
			board[cell + 1].position = sf::Vector2f(cs.x * (x + 1), cs.y * y);
			board[cell + 2].position = sf::Vector2f(cs.x * (x + 1), cs.y * (y + 1));
			board[cell + 3].position = sf::Vector2f(cs.x * x, cs.y * (y + 1));

			pieces[cell    ].position = sf::Vector2f(cs.x * x, cs.y * y);
			pieces[cell + 1].position = sf::Vector2f(cs.x * (x + 1), cs.y * y);
			pieces[cell + 2].position = sf::Vector2f(cs.x * (x + 1), cs.y * (y + 1));
			pieces[cell + 3].position = sf::Vector2f(cs.x * x, cs.y * (y + 1));

			for (unsigned i = 0; i < 4; ++i) {
				board[cell + i].color = (x+y) % 2 == 0 ? sf::Color(200,200,200,255) : sf::Color(50,50,50,255);
			}
			sf::Vector2u vec = sf::Vector2u(x, y);
			if (game.hasPiece(vec)){
				addPiece(game.getPiece(vec),vec);
			}

		}
	}
}

void BoardView::addPiece(Piece & piece, sf::Vector2u pos)
{
	sf::Vector2i offset = sf::Vector2i(0, 0);
	switch (piece.type) {
		case Type::KING:
			offset.x = 0;
			break;
		case Type::QUEEN:
			offset.x = 1;
			break;
		case Type::BISHOP:
			offset.x = 2;
			break;
		case Type::KNIGHT:
			offset.x = 3;
			break;
		case Type::ROOK:
			offset.x = 4;
			break;
		case Type::PAWN:
			offset.x = 5;
			break;
	}
	offset.y = piece.color == WHITE ? 0 : 1;

	unsigned cell = (pos.y * 8 + pos.x) * 4;

	pieces[cell  ].texCoords = sf::Vector2f((offset.x  ) * 100, (offset.y  ) * 100);
	pieces[cell+1].texCoords = sf::Vector2f((offset.x+1) * 100, (offset.y  ) * 100);
	pieces[cell+2].texCoords = sf::Vector2f((offset.x+1) * 100, (offset.y+1) * 100);
	pieces[cell+3].texCoords = sf::Vector2f((offset.x  ) * 100, (offset.y+1) * 100);
}

void BoardView::movePiece(sf::Vector2u pos1, sf::Vector2u pos2)
{
	unsigned cell1 = (pos1.y * 8 + pos1.x) * 4;
	unsigned cell2 = (pos2.y * 8 + pos2.x) * 4;

	for (unsigned i = 0; i < 4; ++i) {
		pieces[cell2 + i].texCoords = pieces[cell1 + i].texCoords;
	}
	rmPiece(pos1);
}

void BoardView::rmPiece(sf::Vector2u pos)
{
	unsigned cell = (pos.y * 8 + pos.x) * 4;
	for (unsigned i = 0; i < 4; ++i) {
		pieces[cell + i].texCoords = sf::Vector2f(0, 0);
	}
}

void BoardView::highlightMoves(Moves moves)
{
	caseEffect.resize(moves.size() * 4);
	unsigned i = 0;
	for (auto& move : moves) {
		sf::Vector2u pos =sf::Vector2u(move.pos2.x*100,move.pos2.y*100);
		caseEffect[i].position = sf::Vector2f(pos.x, pos.y);
		caseEffect[i+1].position = sf::Vector2f(pos.x + 100, pos.y);
		caseEffect[i+2].position = sf::Vector2f(pos.x + 100, pos.y + 100);
		caseEffect[i+3].position = sf::Vector2f(pos.x, pos.y + 100);

		for (unsigned j = 0; j < 4; ++j) {
			caseEffect[i + j].color = sf::Color(30, 30, 255, 128);
		}
		i+=4;
	}
}

void BoardView::removeHighlight()
{
	caseEffect.resize(0);
}

void BoardView::draw()
{
	w->draw(board);
	w->draw(caseEffect);
	w->draw(pieces, &tPieces);
}
