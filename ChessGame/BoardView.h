#pragma once
#include "Game.h"
#include <SFML/Graphics.hpp>

using std::unique_ptr;
class BoardView
{
public:
	Game& game;

	unique_ptr<sf::RenderWindow> w;

	sf::VertexArray board;
	sf::VertexArray caseEffect;
	sf::VertexArray pieces;

	sf::Texture tPieces;



	BoardView(Game& game);

	void init();

	void resetDisplay();

	void addPiece(Piece& piece, sf::Vector2u pos);
	void movePiece(sf::Vector2u pos1, sf::Vector2u pos2);
	void rmPiece(sf::Vector2u pos);

	void highlightMoves(Moves moves);
	void removeHighlight();

	void draw();
};

