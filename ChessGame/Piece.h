#pragma once
#include <vector>
enum Type {
	PAWN,
	ROOK,
	KNIGHT,
	BISHOP,
	QUEEN,
	KING
};
enum Color {
	BLACK,
	WHITE
};
class Piece
{
public:
	Piece(Type type,Color color);
	Color color;
	Type type;
};
typedef std::vector<Piece> Pieces;
