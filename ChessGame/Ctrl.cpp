#include "Ctrl.h"
#include <SFML/Graphics.hpp>
#include <iostream>

Ctrl::Ctrl():game(Game()),boardView(BoardView(game))
{
}
void Ctrl::init()
{
	game.setupGame();
	boardView.init();
	boardView.resetDisplay();

	players.push_back(std::make_unique<Player>(Player(game, HUMAN)));
	players.push_back(std::make_unique<Player>(Player(game, HUMAN)));
	
	selectedPiece.x = 10;
}
void Ctrl::run() {
	sf::RenderWindow* w = boardView.w.get();
	BoardView* v = &boardView;
	Game* g = &game;

	init();

	while (boardView.w->isOpen()) {
		sf::Event event;
		while (w->pollEvent(event))
		{
			if (event.type == sf::Event::Closed ||
			sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ||
			sf::Keyboard::isKeyPressed(sf::Keyboard::F12)) {
				w->close();
			}
		}

		w->clear();
		yield();
		v->draw();

		w->display();
	}
}

void Ctrl::yield()
{
	sf::RenderWindow* w = boardView.w.get();
	BoardView* v = &boardView;
	Game* g = &game;
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {
		selectedPiece.x = 10;
		v->removeHighlight();
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		++clickedFrame;
	} else {
		clickedFrame = 0;
	}
	if (clickedFrame == 1) {
		sf::Vector2i mousePos = sf::Mouse::getPosition(*w);
		sf::Vector2u clickedCase = sf::Vector2u(mousePos.x / 100, mousePos.y / 100);
		if (g->isInside(clickedCase)) {
			if (g->hasPiece(clickedCase) && g->getPiece(clickedCase).color == g->turn) {
				v->highlightMoves(g->getMoves(clickedCase));
				selectedPiece = sf::Vector2u(clickedCase.x, clickedCase.y);
			} else if(selectedPiece.x<10){ // on a selectionné une piece
				if (g->move(Move(selectedPiece, clickedCase))) {
					v->movePiece(selectedPiece, clickedCase);
				}
				else {
					std::cout << "couldnt move";
				}
			}
		}

	}
}
