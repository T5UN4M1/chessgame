#pragma once

#include "Game.h"
#include "BoardView.h"
#include "Player.h"

using std::unique_ptr;
using std::vector;
class Ctrl
{
public:
	Game game;
	BoardView boardView;

	vector<unique_ptr<Player>> players;

	sf::Vector2u selectedPiece;

	int clickedFrame;

	Ctrl();

	void init();
	void run();
	void yield();
};

