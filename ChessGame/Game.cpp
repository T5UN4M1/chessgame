#include "Game.h"
#include <iostream>


Game::Game()
{
}

bool Game::isInside(sf::Vector2u pos) const
{
	return pos.x < 8 && pos.y < 8;
}

bool Game::isInside(unsigned x, unsigned y) const
{
	return isInside(sf::Vector2u(x,y));
}

bool Game::hasPiece(sf::Vector2u pos) const
{
	return matrix.get(pos.x, pos.y).get() != nullptr;
}

bool Game::hasEnemyPiece(sf::Vector2u & pos, Piece & piece) const
{
	return getPiece(pos).color != piece.color;
}

Piece& Game::getPiece(sf::Vector2u pos) const
{
	return *matrix.get(pos.x, pos.y);
}

Moves Game::getMoves(const sf::Vector2u& pos) const
{
	Piece piece = getPiece(pos);
	Moves moves;
	sf::Vector2u c;
	switch (piece.type) {
	case KING:
		for (int x = -1; x < 2; ++x) {
			for (int y = -1; y < 2; ++y) {
				if (x == 0 && y == 0) {
					continue;
				}
				c.x = pos.x + x;
				c.y = pos.y + y;
				if (isInside(c) && (!hasPiece(c) || hasEnemyPiece(c, piece))) {
					moves.push_back(Move(pos, sf::Vector2u(pos.x + x, pos.y + y)));
				}
			}
		}
		break;
	case QUEEN:
		for (int mx = -1; mx < 2; ++mx) {
			for (int my = -1; my < 2; ++my) {
				if (mx == 0 && my == 0) {
					continue;
				}
				for (unsigned i = 1; i < 9; ++i) {
					c = sf::Vector2u(pos.x + i * mx, pos.y + i * my);
					if (isInside(c)) {
						if (hasPiece(c)) {
							if (getPiece(c).color != piece.color) {
								moves.push_back(Move(pos, c));
							}
							break;
						}
						moves.push_back(Move(pos, c));
					}
				}
			}
		}
		break;
	case KNIGHT:
		for (int x = -2; x < 3; ++x) {
			if (x == 0) {
				++x;
			}
			int my = (x*x == 4) ? 1 : 2;
			for (int y = -1; y < 2; y += 2) {
				c = sf::Vector2u(pos.x + x, pos.y + my * y);
				if (isInside(c) && (!hasPiece(c) || hasEnemyPiece(c, piece))){
					moves.push_back(Move(pos, c));
				}
			}
		}
		break;
	case BISHOP:
		for (int mx = -1; mx < 2; mx += 2) {
			for (int my = -1; my < 2; my += 2) {
				for (unsigned i = 1; i < 9; ++i) {
					c.x = pos.x + mx * i;
					c.y = pos.y + my * i;
					if (!isInside(c)) {
						break;
					}
					if (hasPiece(c)) {
						if (getPiece(c).color != piece.color) {
							moves.push_back(Move(pos, c));
						}
						break;
					}
					moves.push_back(Move(pos, c));
				}
			}
		}
		break;
	case ROOK:
		for (int mx = -1; mx < 2; ++mx) {
			for (int my = -1; my < 2; ++my) {
				if (mx*mx == my*my) {
					continue;
				}
				for (unsigned i = 1; i < 9; ++i) {
					c.x = pos.x + mx * i;
					c.y = pos.y + my * i;
					if (!isInside(c)) {
						break;
					}
					if (hasPiece(c)) {
						if (getPiece(c).color != piece.color) {
							moves.push_back(Move(pos, c));
						}
						break;
					}
					moves.push_back(Move(pos, c));
				}
			}
		}
		break;
	case PAWN: {
			int my = piece.color == WHITE ? -1 : 1;
			// taking
			for (int x = -1; x < 2; x += 2) {
				c.x = pos.x + x;
				c.y = pos.y + 1 * my;
				if (hasPiece(c) && hasEnemyPiece(c, piece)) {
					moves.push_back(Move(pos, c));
				}
			}
			bool startingPos = ((piece.color == WHITE && pos.y == 6) || (piece.color == BLACK && pos.y == 1));
			for (int i = 1; i < 3; ++i) {
				if (i == 2 && !startingPos) {
					break;
				}
				c.x = pos.x;
				c.y = pos.y + i * my;
				if (hasPiece(c)) {
					break;
				}
				else {
					moves.push_back(Move(pos, c));
				}
			}
		}
	}
	return moves;
}

bool Game::isLegal(const Move & move) const
{
	if (!isInside(move.pos1) || !isInside(move.pos2)) {
		return false;
	}
	if (!hasPiece(move.pos1) || getPiece(move.pos1).color != turn) {
		return false;
	}
	bool found = false;
	for (auto& mov : getMoves(move.pos1)) {
		if (move.pos2 == mov.pos2) {
			return true;
		}
	}
	return false;

}

bool Game::move(const Move & move)
{
	if (isLegal(move)) {
		Piece* piece = matrix.GET(move.pos1.x, move.pos1.y).get();
		matrix.GET(move.pos1.x, move.pos1.y).reset(nullptr);
		matrix.GET(move.pos2.x, move.pos2.y).reset(piece);
		return true;
	}
	return false;
}

void Game::setupGame()
{
	matrix.GET().clear();
	matrix.setHSize(8);

	for (unsigned i = 0; i < 64; ++i) {
		matrix.GET().push_back(unique_ptr<Piece>(nullptr));
	}
	for (unsigned c = 0; c < 2; ++c) {
		unsigned y = c == 0 ? 0 : 7;
		Color color = c == 0 ? BLACK : WHITE;

		matrix.GET(0, y).reset(new Piece(ROOK, color));
		matrix.GET(1, y).reset(new Piece(KNIGHT, color));
		matrix.GET(2, y).reset(new Piece(BISHOP, color));

		matrix.GET(3, y).reset(new Piece(QUEEN, color));
		matrix.GET(4, y).reset(new Piece(KING, color));

		matrix.GET(5, y).reset(new Piece(BISHOP, color));
		matrix.GET(6, y).reset(new Piece(KNIGHT, color));
		matrix.GET(7, y).reset(new Piece(ROOK, color));
		y = c == 0 ? 1 : 6;
		for (unsigned i = 0; i < 8; ++i) {
			matrix.GET(i, y).reset(new Piece(PAWN, color));
		}

	}

	turn = WHITE;
	moves.clear();
}


