#pragma once
#include "Game.h"
enum PlayerType {
	AI,
	HUMAN
};
class Player
{
public:
	Game& game;
	PlayerType type;
	Pieces takenPieces;
	
	Player(Game& game, PlayerType type);
	virtual Move getMove();
};

