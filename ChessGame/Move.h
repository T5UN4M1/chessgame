#pragma once
#include <SFML/Graphics.hpp>

class Move
{
public:
	sf::Vector2u pos1;
	sf::Vector2u pos2;
	Move(sf::Vector2u pos1,sf::Vector2u pos2);

};
typedef std::vector<Move> Moves;
